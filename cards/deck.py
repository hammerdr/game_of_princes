from random import sample, shuffle

class Card:
    def __init__(self, name, properties=None):
        self.name = name
        self.properties = properties

    def get_property(self, key, default=None):
        if not self.properties or key not in self.properties:
            return default

        return self.properties[key]

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

class Deck:
    def __init__(self, name):
        self.name = name
        self.cards = []
        self.discard = None

    def add(self, card):
        self.cards.append(card)

    def draw(self, k=1):
        if len(self.cards) < k:
            return []

        results = sample(self.cards, k=k)
        for card in results:
            self.discard.add(card)
            self.cards.remove(card)
        return results

    def reset(self):
        self.cards = self.cards + (self.discard.cards if self.discard else [])
        shuffle(self.cards)
        self.discard = Deck("{} Discard".format(self.name))

    def copy(self):
        new_deck = Deck(self.name)
        for card in self.cards + self.discard.cards:
            new_deck.add(card)
        new_deck.reset()
        return new_deck

def build_deck(cards):
    deck = Deck('Ephemeral')
    for card in cards:
        deck.add(card)
    deck.reset()
    return deck
