from .deck import Deck, Card

PRINCE_DECK = Deck("Princes")

PRINCE_JORA = Card("Jora", {"role":"prince","health":3})
PRINCE_LYMIA = Card("Lymia", {"role":"prince","health":3})
PRINCE_YERRYN = Card("Yerryn", {"role":"prince","health":3})
PRINCE_HYRNAM = Card("Hyrman", {"role":"prince","health":3})
PRINCE_NISSAM = Card("Nissam", {"role":"prince","health":3})

PRINCE_DECK.add(PRINCE_JORA)
PRINCE_DECK.add(PRINCE_LYMIA)
PRINCE_DECK.add(PRINCE_YERRYN)
PRINCE_DECK.add(PRINCE_HYRNAM)
PRINCE_DECK.add(PRINCE_NISSAM)
PRINCE_DECK.reset()

KNIGHTS_DECK = Deck("Knights")

KNIGHT_OF_COIN = Card("Knight of Coin", {"role":"knight","health":3})
KNIGHT_OF_CALVARY = Card("Knight of Calvary", {"role":"knight","health":3})
KNIGHT_OF_ARCHERS = Card("Knight of Archers", {"role":"knight","health":3})
KNIGHT_OF_DEFENSE = Card("Knight of Defense", {"role":"knight","health":3})
KNIGHT_OF_HEALING = Card("Knight of Healing", {"role":"knight","health":3})

KNIGHTS_DECK.add(KNIGHT_OF_COIN)
KNIGHTS_DECK.add(KNIGHT_OF_CALVARY)
KNIGHTS_DECK.add(KNIGHT_OF_ARCHERS)
KNIGHTS_DECK.add(KNIGHT_OF_DEFENSE)
KNIGHTS_DECK.add(KNIGHT_OF_HEALING)
KNIGHTS_DECK.reset()

REBELS_DECK = Deck("Rebels")

ISSOTA = Card("Issota", {"role":"rebel","health":3})
PAULIN = Card("Paulin", {"role":"rebel","health":3})
JEREMIAH = Card("Jeremiah", {"role":"rebel","health":3})
THOM = Card("Thom", {"role":"rebel","health":3})
EMY = Card("Emy", {"role":"rebel","health":3})

REBELS_DECK.add(ISSOTA)
REBELS_DECK.add(PAULIN)
REBELS_DECK.add(JEREMIAH)
REBELS_DECK.add(THOM)
REBELS_DECK.add(EMY)
REBELS_DECK.reset()

TRAITORS_DECK = Deck("Traitors")

HUTCHIN = Card("Hutchin", {"role":"traitor","health":3})
AALART = Card("Aalart", {"role":"traitor","health":3})
EVA = Card("Eva", {"role":"traitor","health":3})
MADLIN = Card("Madlin", {"role":"traitor","health":3})
IBBET = Card("Ibbet", {"role":"traitor","health":3})

TRAITORS_DECK.add(HUTCHIN)
TRAITORS_DECK.add(AALART)
TRAITORS_DECK.add(EVA)
TRAITORS_DECK.add(MADLIN)
TRAITORS_DECK.add(IBBET)
TRAITORS_DECK.reset()
