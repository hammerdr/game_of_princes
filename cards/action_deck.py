from .deck import Deck, Card

ACTION_DECK = Deck("Actions")

def build_card(name, properties, n=1):
    card = Card(name, properties)
    for i in range(n):
        ACTION_DECK.add(card)
    return card

SLASH_CARD = build_card("Slash", {"type":"combat","attack":True}, 25)
DODGE_CARD = build_card("Dodge", {"type":"combat","block":True}, 13)

HEALING_WORD_CARD = build_card("Healing Word", {"type":"heal","heal":1,"range":4}, 6)
CURE_WOUNDS_CARD = build_card("Cure Wounds", {"type":"heal","heal":2,"range":0}, 6)

COMPELLED_DUEL_CARD = build_card("Compelled Duel", {"type":"strategy","duel":True}, 3)
BACKSTAB_CARD = build_card("Backstab", {"type":"strategy","delete":True}, 4)
SLEIGHT_OF_HAND_CARD = build_card("Sleight of Hand", {"type":"strategy","steal":True}, 4)
FIREBALL_CARD = build_card("Fireball", {"type":"strategy","damage_all":True}, 2)
RAGE_CARD = build_card("Rage", {"type":"strategy","attack_all":True}, 1)
MASS_HEALING_WORD_CARD = build_card("Mass Healing Word", {"type":"strategy","heal_all":True}, 1)
MERCHANT_CARD = build_card("Merchant", {"type":"strategy","store":True}, 2)
HARVEST_CARD = build_card("Harvest", {"type":"strategy","draw":2}, 2)
HEROES_FEAST = build_card("Heroes Feast", {"type":"strategy","draw":3}, 1)

DAGGER_CARD = build_card("Dagger", {"type": "weapon","reach":1,"unlimited":True}, 2)
SHORTSWORD_CARD = build_card("Shortsword", {"type":"weapon","reach":1}, 3)
LONGSWORD_CARD = build_card("Longsword", {"type":"weapon","reach":2}, 3)
THROWING_KNIVES_CARD = build_card("Throwing Knives", {"type":"weapon","reach":3}, 1)
CROSSBOW_CARD = build_card("Crossbow", {"type":"weapon","reach":4}, 1)
LONGBOW_CARD = build_card("Longbow", {"type":"weapon","reach":5}, 1)

WALLS_CARD = build_card("Walls", {"type":"defense","defense":1}, 2)
PROTECTION_CARD = build_card("Protection", {"type":"defense","protection":True}, 2)

CALVARY_CARD = build_card("Calvary", {"type":"offense","reach":1}, 1)

HOLD_PERSON_CARD = build_card("Hold Person", {"type":"special","capture":True}, 3)

ACTION_DECK.reset()
