from rules import RuleEngine

class HandCard:
    def __init__(self, card):
        self.card = card
        self.is_visible = False

class Player:
    def __init__(self, name, character_card):
        self.name = name
        self.character_card = character_card
        self.health = self.character_card.get_property("health", 0)
        self.hand = []
        self.rule_engine = RuleEngine()
        self.reach = 1
        self.defense = 0
        self.weapon = None
        self.equipment = []

    def is_alive(self):
        return self.health > 0

    def is_dead(self):
        return not self.is_alive()

    def add_card(self, card):
        self.hand.append(HandCard(card))

    def get_actions(self):
        return [card.card.name for card in self.hand]

    def dodge(self):
        card = next(card for card in self.hand if card.card.name == "Dodge")
        self.hand.remove(card)

    def number_of_cards_to_discard(self):
        return len(self.hand) - self.health

    def discard(self, action):
        card = next(card for card in self.hand if card.card.name == action)
        self.hand.remove(card)

    def play(self, action, game):
        card = next(card for card in self.hand if card.card.name == action)
        card.is_visible = True
        self.rule_engine.play(game, self, card)

    def healing_cards_for(self, target):
        return [card.card.name for card in self.hand if card.card.get_property("heal")]

    def equip_weapon(self, weapon_card):
        existing_weapon_reach = self.weapon.get_property("reach", 1) if self.weapon else 1
        self.weapon = weapon_card
        self.reach += (weapon_card.get_property("reach", 1) - existing_weapon_reach)

    def equip(self, card):
        self.equipment.append(card)
        self.reach += card.get_property("reach", 0)
        self.defense += card.get_property("defense", 0)
