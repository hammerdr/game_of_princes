from .player import Player
from cards.action_deck import ACTION_DECK
from cards.character_decks import PRINCE_DECK, KNIGHTS_DECK, REBELS_DECK, TRAITORS_DECK
from cards.deck import build_deck
from random import shuffle
from prompt_toolkit import prompt
from tabulate import tabulate

class Game:
    def __init__(self):
        self.players = []
        self.state = 'init'
        self.substate = 0
        self.deck = ACTION_DECK.copy()

    def add_player(self, player):
        self.players.append(player)

    def is_finished(self):
        return len([player for player in self.players if player.is_alive()]) <= 1

    def prompt(self, message):
        return prompt(message)

    def request_target(self):
        response = prompt('Who is your target? > ')
        target = next(player for player in self.players if player.name == response)
        if target:
            return target
        else:
            print("They don't seem to be here.")
            self.request_target()

    def distance_from(self, player, target):
        player_index = self.players.index(player)
        target_index = self.players.index(target)
        neg_player_index = player_index - len(self.players)
        neg_target_index = target_index - len(self.players)
        return min(abs(player_index - target_index), abs(player_index - neg_target_index), abs(target_index - neg_player_index))

    def print_visible_game_state(self):
        print(tabulate([[player.name, (player.health if player.is_alive() else "dead"), ("prince" if player.character_card.get_property("role") == "prince" else "")] for player in self.players], headers=["Player", "Health", "Role"], tablefmt="github"))

    def get_prompt(self):
        if self.state == 'init':
            return "How many players? > "
        if self.state == 'player_creation':
            return "What is the name of player {}? > ".format(self.substate + 1)
        if self.is_finished():
            return "The game is finished! Type exit to leave. > "
        if self.state == 'play_game':
            current_player = self.players[self.substate]
            if self.phase == 'draw':
                cards = self.deck.draw(2)
                for card in cards:
                    current_player.add_card(card)
                return "{} drew 2 cards. Continue? > ".format(current_player.name)
            elif self.phase == 'discard':
                return "{} must discard {} cards. Which card do you want to discard? > ".format(current_player.name, current_player.number_of_cards_to_discard())

            return "Which card do you want to play? Indicate done with 'done' > "

        return "Enter to advance to the next step. > "

    def advance(self, response):
        if self.state == 'init':
            self.number_of_players = int(response)
            self.state = 'player_creation'

            if self.number_of_players == 2:
                print("For 2 players, there is 1 Prince and 1 Rebel.")
                self.character_deck = build_deck(PRINCE_DECK.draw(1) + REBELS_DECK.draw(1))
            elif self.number_of_players == 3:
                print("For 3 players, there is 1 Prince, 1 Rebel, and 1 Traitor.")
                self.character_deck = build_deck(PRINCE_DECK.draw(1) + REBELS_DECK.draw(1) + TRAITORS_DECK.draw(1))
            elif self.number_of_players == 4:
                print("For 4 players, there is 1 Prince, 1 Knight, 1 Rebel, and 1 Traitor.")
                self.character_deck = build_deck(PRINCE_DECK.draw(1) + KNIGHTS_DECK.draw(1) + REBELS_DECK.draw(1) + TRAITORS_DECK.draw(1))
            elif self.number_of_players == 5:
                print("For 5 players, there are 1 Prince, 1 Knight, 2 Rebels, and 1 Traitor.")
                self.character_deck = build_deck(PRINCE_DECK.draw(1) + KNIGHTS_DECK.draw(1) + REBELS_DECK.draw(2) + TRAITORS_DECK.draw(1))
            elif self.number_of_players == 6:
                print("For 6 players, there are 1 Prince, 1 Knight, 3 Rebels, and 1 Traitor.")
                self.character_deck = build_deck(PRINCE_DECK.draw(1) + KNIGHTS_DECK.draw(1) + REBELS_DECK.draw(3) + TRAITORS_DECK.draw(1))
            elif self.number_of_players == 7:
                print("For 7 players, there are 1 Prince, 2 Knights, 3 Rebels, and 1 Traitor.")
                self.character_deck = build_deck(PRINCE_DECK.draw(1) + KNIGHTS_DECK.draw(2) + REBELS_DECK.draw(3) + TRAITORS_DECK.draw(1))
            else:
                self.state = 'init'
                print("This game can be played for 2-7 players.")
            return

        if self.state == 'player_creation':
            print("Creating a player named {}".format(response))
            player = Player(response, self.character_deck.draw()[0])
            cards = self.deck.draw(4)
            for card in cards:
                player.add_card(card)
            self.add_player(player)
            self.substate += 1
            if self.substate >= self.number_of_players:
                shuffle(self.players)
                self.state = 'play_game'
                self.substate = 0
                self.phase = 'draw'
                return

        if self.state == 'play_game':
            current_player = self.players[self.substate]
            if self.phase == 'draw':
                self.print_visible_game_state()
                self.phase = 'action'
                return
            if response == 'list':
                print("\n".join([" * {}".format(action) for action in current_player.get_actions()]))
                return
            if self.phase == 'discard':
                if response == 'done':
                    if current_player.number_of_cards_to_discard() > 0:
                        print('You must discard another card')
                        return
                    elif current_player.number_of_cards_to_discard() == 0:
                        self.phase = 'draw'
                        self.substate = (self.substate + 1) % self.number_of_players
                        return
                if response in current_player.get_actions():
                    current_player.discard(response)
                    return
                print("That action is not available.")
                return
            if response == 'done':
                self.phase = 'discard'
                return
            if response in current_player.get_actions():
                current_player.play(response, self)
                return
            print("That action is not available.")
