from prompt_toolkit import prompt
import sys
from engine.game import Game

game = Game()

print("Welcome to Game of Princes!")
print('Use command "exit" to quit. Anything else will start the game.')

while 1:
    user_input = prompt(game.get_prompt())

    if user_input == "exit":
        print("Exiting...")
        sys.exit(0)

    game.advance(user_input)
