def run_compelled_duel(game, player, card):
    pass

def run_delete(game, player, card):
    pass

def run_steal(game, player, card):
    pass

def run_damage_all(game, player, card):
    pass

def run_attack_all(game, player, card):
    pass

def run_mass_healing_word(game, player, card):
    pass

def run_merchant(game, player, card):
    pass

def run_draw(game, player, card):
    number_to_draw = card.card.get_property('draw', 0)
    player.hand.remove(card)
    cards = game.deck.draw(number_to_draw)
    for card in cards:
        player.add_card(card)
