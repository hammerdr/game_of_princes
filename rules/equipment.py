def run_weapon(game, player, card):
    player.hand.remove(card)
    player.equip_weapon(card.card)
    print("{} wields a {}".format(player.name, card.card.name))

def run_equip(game, player, card):
    player.hand.remove(card)
    player.equip(card.card)
    print("{} equips a {}".format(player.name, card.card.name))
