def run_death_save(game, player):
    response = game.prompt("Who would like to save {}? > ".format(player.name))
    if response == "no" or response == "noone":
        return

    saver = next(p for p in game.players if p.name == response)
    if len(saver.healing_cards_for(player)) > 0:
        heal = game.prompt("Which heal would they like to play? > ")
        if heal in saver.healing_cards_for(player):
            saver.play(heal, game)
            return

    print("They cannot do that!")
    run_death_save(game, player)

def run_slash(game, player, card):
    target = game.request_target()
    distance = game.distance_from(player, target)
    if (distance + target.defense) > player.reach:
        print("That target is too far away!")
        return

    response = game.prompt("Does {} want to dodge? > ".format(target.name))
    if (response == 'y' or response == 'yes') and "Dodge" in target.get_actions():
        target.dodge()
        print("{} has dodged {}'s Slash!".format(target.name, player.name))
        return

    player.hand.remove(card)
    target.health -= 1
    print("{} has {} remaining health!".format(target.name, target.health))

    if target.is_dead():
        run_death_save(game, target)

def run_healing_word(game, player, card):
    player.hand.remove(card)
    target = game.request_target()
    target.health += 1
    print("{} has {} health!".format(target.name, target.health))

def run_cure_wounds(game, player, card):
    player.hand.remove(card)
    player.health += 2
    print("{} has {} health!".format(player.name, player.health))
