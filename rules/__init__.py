from .basic import run_slash, run_healing_word, run_cure_wounds
from .equipment import run_weapon, run_equip
from .strategy import run_draw, run_merchant, run_mass_healing_word, run_attack_all, run_damage_all, run_steal, run_delete, run_compelled_duel

RULES = {
    'Slash': run_slash,
    'Healing Word': run_healing_word,
    'Cure Wounds': run_cure_wounds,

    'Dagger': run_weapon,
    'Shortsword': run_weapon,
    'Longsword': run_weapon,
    'Throwing Knives': run_weapon,
    'Crossbow': run_weapon,
    'Longbow': run_weapon,

    'Walls': run_equip,
    'Protection': run_equip,
    'Calvary': run_equip,

    'Compelled Duel': run_compelled_duel,
    'Backstab': run_delete,
    'Sleight of Hand': run_steal,
    'Fireball': run_damage_all,
    'Rage': run_attack_all,
    'Mass Healing Word': run_mass_healing_word,
    'Merchant': run_merchant,

    'Harvest': run_draw,
    'Heroes Feast': run_draw,
}

class RuleEngine:
    def play(self, game, player, card):
        run_rule = RULES[card.card.name]
        if not run_rule:
            print("Rule for card {} not found.".format(card.card.name))
            return

        run_rule(game, player, card)
